#!/usr/bin/env perl
use strict;
use warnings;
use 5.10.0;

use Carp;
use Cpanel::JSON::XS;
use Date::Parse;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use Getopt::Long;

my $dir     = 'json';
my $output  = 'data.json';
my $max_age = 1;
my ($mail, $from, $even_if_ok, $print_result, $help);

GetOptions (
    'help'         => \$help,
    'dir=s'        => \$dir,
    'output=s'     => \$output,
    'max-age=i'    => \$max_age,
    'mail=s'       => \$mail,
    'from=s'       => \$from,
    'even-if-ok'   => \$even_if_ok,
    'print-result' => \$print_result
);

if ($help) {
    print <<EOF;
(c) 2019 Framasoft, GPLv3

Purpose: read json outputs from `borg list`, check that latest backups are not older than X days and create a JSON file that you can use with https://framagit.org/framasoft/borgbackup/borg-dashboard-vue. It can also send mails to alert you if your backups are too old.

Dependencies:
  — Cpanel::JSON::XS. Install it from CPAN or install libcpanel-json-xs-perl on Debian
  — Date::Parse. Install it from CPAN or install libtimedate-perl on Debian
  — MIME::Lite. Install it from CPAN or install libmime-lite-perl on Debian.
    This an optional dependency, only needed if you use the --mail option
  — Mail::Valid. Install it from CPAN or install libemail-valid-perl on Debian
    This an optional dependency, only needed if you use the --mail option

How to use:
 1. Do `borg list --json REPOSITORY > json/new.json`
    You can do it for many repositories, just give each output a unique name.
    Each file must have `.json` suffix.
 2. Do `./dashboard.pl --dir json/`
 3. Enjoy your JSON file ($output)

Usage:
  ./dashboard.pl [--help] [--dir <directory containing JSON outputs of listing>] [--output <filename>]
                          [--max-age <maximum age of last backup>] [--mail <email address> --from <email address> [--even-if-ok]]
                          [--print-result [--even-if-ok]]

  --help:        print this help and exit
  --dir:         directory where to find json outputs. Default to 'json/'
  --output:      filename of the JSON output, default is 'data.json'
  --max-age:     maximum age of last backup, in days, default is 1 day
  --mail:        email address which will receive a alert if some backups are too old
  --from:        email address which will send the alert if some backups are too old
  --print-result print the result of the check (same as the mails content) on the standard output.
  --even-if-ok:  with --mail, send mail even if all servers have up-to-date backups (useful to be sure that the script works well)
                 with --print-result print result even if all servers have up-to-date backups
EOF
    exit 0
}

## Pre flight check
die sprintf('Directory (%s) is not readable. Exiting', $dir) unless -r $dir;
my $dir_output = dirname($output);
die sprintf('%s is not a directory. Exiting', $dir_output)          unless -d $dir_output;
die sprintf('Directory (%s) is not writable. Exiting', $dir_output) unless -w $dir_output;
if (($mail || $from ) && $even_if_ok) {
    die 'You need to set the recipient’s email address with --mail option' unless $mail;
    die 'You need to set the sender’s email address with --from option' unless $from;

    require Email::Valid;

    Email::Valid->address($mail) or die sprintf('The recipient’s email address (%s) is not valid. Exiting', $mail);
    Email::Valid->address($from) or die sprintf('The sender’s email address (%s) is not valid. Exiting', $from);
}

## Get files list
my @files = glob(File::Spec->catfile($dir, '*.json'));

## Read files
my %servers = ();
for my $file (@files) {
    open my $fh, '<', $file or die sprintf('Unable to open %s : %s', $file, $!);
    local $/ = undef;
    my $content = <$fh>;
    close $fh;

    my $name = fileparse($file, ('.json'));

    if ($content) {
        my $json = decode_json $content;

        my $ref = ref $json;
        if ($ref eq 'ARRAY') {
            $servers{$name} = $json->[0];
        } elsif ($ref eq 'HASH') {
            $servers{$name} = $json;
        } else {
            say STDERR sprintf('%s seems to not be a correct borg list export. Ignoring.', $file);
        }
    } else {
        $servers{$name} = {};
    }
}

## Extract informations
my %dashboard = ( too_old => { count => 0, servers => [] }, servers => {} );
my @too_old_backups;
my $now = time;
for my $server (sort keys %servers) {
    my $too_old = 1;

    $dashboard{servers}->{$server} = { backups => [] };

    if (defined $servers{$server}->{archives}) {
        for my $archive (@{$servers{$server}->{archives}}) {
            my $time = str2time($archive->{time});
            push @{$dashboard{servers}->{$server}->{backups}}, $time;

            # Check backup age
            if ($time >= $now - $max_age * 86400) {
                $too_old = 0;
            }
        }
    }

    $dashboard{servers}->{$server}->{too_old} = $too_old;
    push @too_old_backups, $server if $too_old;

}

$dashboard{too_old}->{count}   = scalar(@too_old_backups);
$dashboard{too_old}->{servers} = \@too_old_backups;
$dashboard{timestamp}          = time;

open my $fh, '>', $output;
print $fh encode_json \%dashboard;
close $fh;

my $too_old_backups_nb = scalar(@too_old_backups);
if (($print_result || $mail) && ($too_old_backups_nb || $even_if_ok)) {
    my $servers_nb = scalar(keys %servers);
    my $a = ($servers_nb > 1) ? 'servers' : 'server';
    my $d = ($max_age    > 1) ? 'days'    : 'day';

    my $subject = 'Borg backups’ age check: ';
    my $msg;
    if ($too_old_backups_nb) {
        my $s = ($too_old_backups_nb > 1) ? 'servers' : 'server';
        my $names = join("\n", map { sprintf('- %s', $_) } @too_old_backups);

        $subject .= sprintf('WARNING %i %s without fresh backups', $too_old_backups_nb, $s);
        $msg = <<EOF;
On $servers_nb $a, I detected a problem on $too_old_backups_nb $s. :-(
Please check the backups of the following $s which all backups are older than $max_age $d:
$names
EOF
    } else {
        $subject .= 'all is good';
        $msg = <<EOF;
Nothing to worry about :-)

$servers_nb $a have at least one backup younger than $max_age $d.
EOF
    }

    if ($print_result) {
        print sprintf("%s\n\n%s", $subject, $msg);
    }
    if ($mail) {
        require MIME::Lite;

        $msg = MIME::Lite->new(
            From     => $from,
            To       => $mail,
            Subject  => $subject,
            Data     => $msg
        );
        $msg->attr("content-type" => 'text/plain; charset="UTF-8"');
        $msg->send;
    }
}
