# Borg dashboard exporter

It reads JSON outputs from `borg list`, check that latest backups are not older than X days and create a JSON file suitable for [our borg web dashboard](https://framagit.org/framasoft/borgbackup/borg-dashboard-vue).

It can also send mails to alert you if your backups are too old.

## Dependencies
- Cpanel::JSON::XS. Install it from CPAN or install libcpanel-json-xs-perl on Debian.
- Date::Parse. Install it from CPAN or install libtimedate-perl on Debian.
- MIME::Lite. Install it from CPAN or install libmime-lite-perl on Debian. This an optional dependency, only needed if you use the --mail option
- Mail::Valid. Install it from CPAN or install libemail-valid-perl on Debian. This an optional dependency, only needed if you use the --mail option

## How to use

 1. Do `borg list --json REPOSITORY > json/new.json`
    You can do it for many repositories, just give each output a unique name.
    Each file must have `.json` suffix.
 2. Do `./dashboard.pl --dir json/`
 3. Enjoy your JSON output

## Usage:

```
  ./dashboard.pl [--help] [--dir <directory containing JSON outputs of listing>] [--output <filename>]
                          [--max-age <maximum age of last backup>] [--mail <email address> --from <email address> [--even-if-ok]]
                          [--print-result [--even-if-ok]]

  --help:        print this help and exit
  --dir:         directory where to find json outputs. Default to 'json/'
  --output:      filename of the JSON output, default is 'data.json'
  --max-age:     maximum age of last backup, in days, default is 1 day
  --mail:        email address which will receive a alert if some backups are too old
  --from:        email address which will send the alert if some backups are too old
  --print-result print the result of the check (same as the mails content) on the standard output.
  --even-if-ok:  with --mail, send mail even if all servers have up-to-date backups (useful to be sure that the script works well)
                 with --print-result print result even if all servers have up-to-date backups
```

## How to use with our borg web dashboard?

Fetch the [latest release](https://framasoft.frama.io/borgbackup/borg-dashboard-vue/borg-dashboard-latest.zip) of the dashboard:

```
cd /var/www/
wget https://framasoft.frama.io/borgbackup/borg-dashboard-vue/borg-dashboard-latest.zip
unzip borg-dashboard-latest.zip
rm borg-dashboard-latest.zip
```

Install and use the exporter:

```
cd /opt
git clone https://framagit.org/framasoft/borgbackup/borg-dashboard-exporter.git
cd borg-dashboard-exporter
./dashboard.pl --dir /your/borg/list/output/directory/ --output /var/www/borg-dashboard/data.json
```

That’s all 🙂

**NB**: if you need to serve the dashboard on a different URL than `/`, you’ll need to compile it yourself. See the instructions on [the web dashboard project page](https://framagit.org/framasoft/borgbackup/borg-dashboard-vue#compiles-and-minifies-for-production).

## LICENSE
© 2019 Framasoft, GPLv3

See [LICENSE](LICENSE) file.
